package com.neusoft.springdemo;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class SpringContextTest {
    private Logger logger = Logger.getLogger(SpringContextTest.class);
    private ApplicationContext ctx;

    @Before
    public void init() {
        String configLocation = "applicationContext.xml";
        ctx = new ClassPathXmlApplicationContext(configLocation);
        logger.info("成功加载spring运行环境");
    }

    @Test
    public void testContext() {
        Date now = null;

        now = ctx.getBean(Date.class);

        //now = new Date();

        logger.info(now.toString());

    }
}
