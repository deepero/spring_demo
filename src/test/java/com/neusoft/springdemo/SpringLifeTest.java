package com.neusoft.springdemo;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringLifeTest {

    private Logger logger = Logger.getLogger(SpringLifeTest.class);
    private ApplicationContext ctx;

    @Before
    public void init() {
        String configLocation = "bean.xml";
        ctx = new ClassPathXmlApplicationContext(configLocation);
        logger.info("成功加载spring运行环境");
    }


    @Test
    public void testLife(){
        logger.info("spring running......");
    }
}
