package com.neusoft.springdemo;

import com.neusoft.springdemo.bean.User;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserTest {
    private static Logger logger = Logger.getLogger(UserTest.class);
    private static ApplicationContext ctx = null;


    @BeforeClass
    public static void init(){
        logger.info("starting.......");
        String configLocation = "applicationContext.xml";
        ctx = new ClassPathXmlApplicationContext(configLocation);
        logger.info("spring 环境加载完毕");
    }

    @Test
    public void testUser(){
//        类型强转本质是类型还原
        User user = (User)ctx.getBean("user");
        logger.info(user);
        logger.info("程序加载结束");

    }

    @Test
    public void testUser2(){
//        类型强转本质是类型还原
        User user = (User)ctx.getBean("user2");
        logger.info(user);
        logger.info("程序加载结束");

    }
}
