package com.neusoft.springdemo.bean;


import org.apache.log4j.Logger;

public class Product {
    private Logger logger = Logger.getLogger(Product.class);
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        logger.info("setUser(User user)......");
    }

    public Product() {
        logger.info("Product()......");
    }

    public Product(User user) {
        this.user = user;
        logger.info("Product(User user)......");
    }
}
