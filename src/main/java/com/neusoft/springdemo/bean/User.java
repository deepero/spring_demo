package com.neusoft.springdemo.bean;

import com.sun.deploy.panel.IProperty;
import org.apache.log4j.Logger;

import java.util.*;

public class User {
    private Integer userId;
    private  String userName;
    private boolean isMan;
    private Date regDate;
    private List<String> hobbyList;
    private Set<String> majorSet;
    private Map<String,Double> gradeMap;
    private Properties jdbcProperties;
    private int[] ids;
    private Logger logger = Logger.getLogger(User.class);


    public User() {
        logger.info("User()");
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(String userName) {
        this.userName = userName;
    }

    public User(Integer userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public User(Integer userId, String userName, Date regDate) {
        this.userId = userId;
        this.userName = userName;
        this.regDate = regDate;
    }

    public User(Integer userId, String userName, boolean isMan) {
        this.userId = userId;
        this.userName = userName;
        this.isMan = isMan;
    }

    public User(Integer userId, String userName, boolean isMan, Date regDate) {
        this.userId = userId;
        this.userName = userName;
        this.isMan = isMan;
        this.regDate = regDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setMan(boolean man) {
        isMan = man;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }



    public List<String> getHobbyList() {
        return hobbyList;
    }

    public void setHobbyList(List<String> hobbyList) {
        this.hobbyList = hobbyList;
    }

    public Set<String> getMajorSet() {
        return majorSet;
    }

    public void setMajorSet(Set<String> majorSet) {
        this.majorSet = majorSet;
    }


    public Map<String, Double> getGradeMap() {
        return gradeMap;
    }

    public void setGradeMap(Map<String, Double> gradeMap) {
        this.gradeMap = gradeMap;
    }


    public Properties getJdbcProperties() {
        return jdbcProperties;
    }

    public void setJdbcProperties(Properties jdbcProperties) {
        this.jdbcProperties = jdbcProperties;
    }


    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", isMan=" + isMan +
                ", regDate=" + regDate +
                ", hobbyList=" + hobbyList +
                ", majorSet=" + majorSet +
                ", gradeMap=" + gradeMap +
                ", jdbcProperties=" + jdbcProperties +
                ", ids=" + Arrays.toString(ids) +
                '}';
    }
}
