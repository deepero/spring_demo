package com.neusoft.springdemo.service.aop;

import org.apache.log4j.Logger;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;

import java.lang.reflect.Method;


/**
 *
 * 使用配置文件Spring-core.xml进行Spring操作
 * */

public class TransactionAop implements MethodBeforeAdvice, AfterReturningAdvice, ThrowsAdvice {

    private Logger logger = Logger.getLogger(TransactionAop.class);

    @Override
    public void before(Method method, Object[] args, Object target){
        logger.info("开启数据库事务");
    }

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        logger.info("提交数据库事务");

    }

    public void afterThrowing(Exception ex) {

        logger.info("回滚数据库事务");
    }
}
